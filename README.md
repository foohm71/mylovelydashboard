# My Lovely Dashboard

This project gives an example of how you can use a Jupyter Notebook and Gitlab Pages to generate an Engineering dashboard.

## How to view the dashboard (generated once a day)

See https://foohm71.gitlab.io/mylovelydashboard/dashboard.html 

## Why use a Jupyter notebook?

There are a lot of python libraries that abstract out APIs to standard tools like Github, Gitlab, Jira, SonarQube etc. 

As a tool to rapidly explore an API and to prototype code in Python, Jupyter notebooks are excellent. The issue is then how do you deploy it with minimal changes? 

## Why Gitlab Pages?

Gitlab pages is a very simple tool to generate and publish static HTML. Thus all we have to do is to use the Python APIs, get the data we need, format it in a way that is useful then somehow publish the data as a static HTML. 

For more information on Gitlab pages, you can see https://docs.gitlab.com/ee/user/project/pages/ 

## The high level workflow

1. Use the Python API to login to system metrics is residing eg. Gitlab
2. Pull the necessary data into a Pandas dataframe
3. Do the necessary processing
4. Create the display object eg. a chart, a table or a value
   * If it is a chart, generate a .png file
   * If it is a table, put the table object into the dictionary object 
   * If it is a value, put it in the dictionary object
5. Once it's time to render, merge the HTML template with the dictionary object

### Steps 1 to 4

All these can be found in ``createDashboard.ipynb``. For those familiar with Jupyter notebooks this is straightforward. 

### Step 5

In step 5 we use the Mako templating engine to populate the dictionary object (see ``createDashboard.ipynb``, the dictionary object is ``values``). For more information on Mako see https://www.makotemplates.org/ 

## Some things to note

Here are some things to note about automated generation of these reports:
* When prototyping the dashboard, I use a .env file to start ENV VARs like API tokens. In Gitlab, you store these as K/V pairs in ``Settings -> CI/CD``. 
* The ``.gitlab-ci.yml`` is very straightforward and shows you how to (a) convert the .ipynb into a .py file, (b) remove the unwanted lines in the code, (c) generate the HTML dashboard and (d) publish it as Gitlab page
* To schedule page generation, see ``CI/CD -> Schedules`` and schedule away! 
  
## Credits

In order to demonstrate these capabilities, I have use 2 open source repositories:
* For Gitlab stats, I've used https://gitlab.com/nomadic-labs/tezos-indexer 
* For SonarQube stats, I've used https://sonarcloud.io/dashboard?id=simgrid_simgrid 